
function de (id) { return document.getElementById(id)}
const minABI = JSON.parse(de('abi').innerText)

var BN = BigNumber;

var gate      =  '0x3e7f12854d37a961e8cd286f1df206a573f74aba';
var contract  =  '0x3B5D98DB33C4f6BD1FB8D566393C07fca712e603';
var tokenDecimals = BN(18);

var app = new Vue({
  el: '#app',
    methods: {
        start: function (){
            var token = new web3.eth.Contract(minABI,contract);
            token.methods.balanceOf(gate).call().then(function (balance) {
                b = BN(balance);
                b = b.div(BN(10).pow(tokenDecimals));
                app.supply = b.toFormat(0);
            })

        },
        send:function (){
            var v = web3.utils.toWei(this.amount.toString(), 'ether') 
            var token = new web3.eth.Contract(minABI,contract);
            var tx = token.methods.gateway(gate,v,this.bis_address);
            tx.send({from : ethereum.selectedAddress, gas: 50000});
        }
    },
    data: {
        amount:'0',
        supply:'0',
        bis_address:''
    }
})

window.addEventListener('load', async () => {
    if (window.ethereum) {
        window.web3 = new Web3(ethereum);
        try {
            await ethereum.enable();
            app.start();
        } catch (error) {

        }
    }
    else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
        window.web3 = new Web3("https://mainnet.infura.io/v3/8dc25d306bee4ee18ba6eafe624394f9");
        app.start();
    }
});
